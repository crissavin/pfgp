% Demo: estimation of traditional 2D place field in square box using a SM
% kernel with extrapolation (see Savin and Tkacik, NIPS 2016 for details)
% Cristina Savin March, 2017
clear,clc
rng(1234, 'twister')

n1=100;n2=100;
[X,Y] = meshgrid(1:n1,1:n2); %2d grid
%% generate a (neurally realistic) random tuning function
% figure, hold on
lbda = 20; Ng = 5;
xodd = (1:Ng)*lbda -15;
% y = lbda*sin(pi/3)*(1:Ng+1)-5;

fr = zeros(n1,n2);
sd = rand-0.5; %allow for grid distortion
sig = [1 sd;sd  1.7]/20;

for i = 1:Ng
    for j = 1:Ng
        mux = xodd(i);
        muy = xodd(j) +  mod(i,2)*lbda*cos(pi/3);
        xx = [X(:)-mux,Y(:)-muy];
        fr = fr+reshape(exp(-sum(xx'.*(sig*xx'))'),n1,n2);
    end
end

frtrue = 0.1+5*fr/max(fr(:));
%% sample some spike counts data (Poisson noise, known tuning function)
T = 10000;
x0=[0.1 0.1]';
x = 0.5*getRandomWalkRing(0.05, T , 0.1,0.95,[0 0.75])+0.5; %x rescaled to unit square!!!
x1 = linspace(0,1.0001,n1+1);
x2 = linspace(0,1.0001,n2+1);
i = ceil((x(:,1)-x1(1))/(x1(2)-x1(1)));
j = ceil((x(:,2)-x2(1))/(x2(2)-x2(1)));
z = poissrnd(frtrue(sub2ind([n1,n2],i,j)));

%% estimate fields using spectral mixture kernel
[pf,hyp2,counts,ll] = getPFGPGrid2D(z,x,n1,n2,0);

%%
figure
subplot(1,4,1)
plot(x(:,1),x(:,2))
axis square
title('random walk')

subplot(1,4,2)
imagesc(frtrue)
axis off
axis square
title('ground truth')

subplot(1,4,4)
imagesc(pf.mtuning)
axis off
axis square
title('estimated tuning PFGP')


subplot(1,4,3)
imagesc(counts)
axis off
axis square
title('basic histogram-based estimate, no smoothing')