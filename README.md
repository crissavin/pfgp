# pfgp

This is a small-scale demo for estimating multidimensional receptive fields in a generative model with 
Poisson noise and a gaussian process prior with Kronecker covariance structure for the log firing rate.

To get started, add gpml library to the path and run  demoEstimation.m 
