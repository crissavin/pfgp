% Demo: estimation of traditional 2D place field in square box using a SE
% kernel (just smoothing, no extrapolation)
% (see Savin and Tkacik, NIPS 2016 for details)
% Cristina Savin, March 2017
clear,clc
rng(1234, 'twister')
n1=100;n2=100;
[X,Y] = meshgrid(1:n1,1:n2); %2d grid
mask = zeros(size(X)); mask(1:5:99,1:5:99)=1; %which subset of the space gets actually visited
%% generate a (neurally realistic) random tuning function 
Ng = 2; % no. pf. peaks
fr = zeros(n1,n2);
for i = 1:Ng
    mux = ceil(100*rand);
    sd = rand-0.5;
    sig = [1 sd;sd  1.7]/(120*(1+rand));
    muy = ceil(100*rand); % place field peaks
    xx = [X(:)-mux,Y(:)-muy];
    fr = fr+reshape(exp(-sum(xx'.*(sig*xx'))'),n1,n2);
end
frtrue = 0.1+5*fr/max(fr(:));
%% sample some spike counts data (Poisson noise, known tuning function)
T = 10000;
x0=[0.1 0.1]';
x = getRandomWalk(0.1, T, x0); %x rescaled to unit square!!!
x1 = linspace(0,1.0001,n1+1); 
x2 = linspace(0,1.0001,n2+1);
i = ceil((x(:,1)-x1(1))/(x1(2)-x1(1)));
j = ceil((x(:,2)-x2(1))/(x2(2)-x2(1)));
z = poissrnd(frtrue(sub2ind([n1,n2],i,j)));

%% Do the estimation
[pf,hyp2,counts,ll] = getPFGPGrid2D(z,x,n1,n2);

%% plot results
figure
subplot(1,4,1)
plot(x(:,1),x(:,2))
axis square
title('random walk')

subplot(1,4,2)
imagesc(frtrue)
axis off
axis square
title('ground truth')

subplot(1,4,4)
imagesc(pf.mtuning)
axis off
axis square
title('estimated tuning PFGP')

subplot(1,4,3)
imagesc(counts)
axis off
axis square
title('basic histogram-based estimate, no smoothing')
