function [pf,hyp2,counts,ll] = getPFGPGrid2D(z,x,n1,n2, useSE)
%inputs: spike counts as function of position
% n1,n2: grid dimensions
if nargin < 4, n2 = n1; end
if nargin <5, useSE=1; end

% create the "computational grid"
x1 = linspace(0,1.0001,n1+1); 
x2 = linspace(0,1.0001,n2+1);

xg = {(x1(1:n1)+x1(2:n1+1))'/2, (x2(1:n2)+x2(2:n2+1))'/2};

i = ceil((x(:,1)-x1(1))/(x1(2)-x1(1)));
j = ceil((x(:,2)-x2(1))/(x2(2)-x2(1)));
counts = full(sparse(i,j,z,n1,n2))./full(sparse(i,j,1,n1,n2));
xx = sub2ind([n1,n2],i,j);

% setup the GP
% cv = {@covProd,{{@covMask,{1,@covSEiso}},{@covMask,{2,@covSEiso}}}};
if useSE
    cvGrid = {@covGrid, {@covSEiso,  @covSEiso}, xg};
    hyp0.cov = log([.1  1 .1 1]);
else
    D = 1; Q = 5; w = ones(Q,1)/Q; m1 = rand(D,Q); v1 = rand(D,Q);
    m2 = rand(D,Q); v2 = rand(D,Q);
    csm = {@covSM,Q};
    cvGrid = {@covGrid, {csm, csm}, xg};
    hyp0.cov = [log([w;m1(:);v1(:)]);log([w;m2(:);v2(:)])];    % Spectral Mixture
end
hyp0.mean = .005;

X = covGrid('expand', cvGrid{3});
lik = {@likPoisson, 'exp'};

tic
hyp2 = minimize(hyp0, @gp, -100, @infGrid_Laplace, @meanConst, cvGrid, lik, xx, z);
toc
ll = gp(hyp2, @infGrid_Laplace, @meanConst, cvGrid, lik, xx, z)/length(z);
fprintf('Log-likelihood learned with Kronecker: %.04f\n',ll);

% posterior predictions
tic
[Ef2, Varf2, fmu2, fs2] = gp(hyp2, @infGrid_Laplace, @meanConst, cvGrid, lik, xx, z, X);
toc

pf.ml =reshape(Ef2,n1,n2); %mean and variance for log f
pf.varl = reshape(Varf2,n1,n2);
pf.mp = reshape(fmu2,n1,n2); % mean and variance of observations
pf.varp = reshape(fs2,n1,n2);
pf.mtuning= reshape(exp(fmu2 +fs2/2),n1,n2); % mean and variance of f (log normal)
pf.vartuning = reshape((exp(fs2)-1).*exp(2*fmu2+fs2),n1,n2);
