%% inference 3d function with subset of grid only, 
%  modified version (original by A.Wilson)
N = 100;
x = rand(N,1); 
idx = rand(N,1)<0.25; x(idx)=     0.25*rand(sum(idx),1);
idx = rand(N,1)<0.3; x(idx)= 0.7+0.25*rand(sum(idx),1);
y = rand(N,1);
idx = rand(N,1)<0.35; y(idx)= 0.75+0.25*rand(sum(idx),1);
z = rand(N,1);
idx = rand(N,1)<0.42; z(idx)= 0.5+0.3*rand(sum(idx),1);

xy = [x,y,z];
figure
plot(x,z,'.')
axis square

%% create the "computational grid"
n1 = 10; n2 = 10; n3 = 10;
x1 = linspace(0,1.0001,n1+1); x2 = linspace(-0.0001,1,n2+1);
x3 = linspace(0.0001,1,n3+1);
xg = {(x1(1:n1)+x1(2:n1+1))'/2, (x2(1:n2)+x2(2:n2+1))'/2,(x3(1:n3)+x3(2:n3+1))'/2};

%% get some data
i = ceil((xy(:,1)-x1(1))/(x1(2)-x1(1)));
j = ceil((xy(:,2)-x2(1))/(x2(2)-x2(1)));
k = ceil((xy(:,3)-x3(1))/(x3(2)-x3(1)));

counts = zeros(n1,n2,n3);
for ik = 1:n3
    counts(:,:,ik) = full(sparse(i(k==ik),j(k==ik),1,n1,n2));
end

%% setup the GP
cv = {@covProd,{{@covMask,{1,@covSEiso}},{@covMask,{2,@covSEiso}},{@covMask,{3,@covSEiso}}}};
cvGrid = {@covGrid, {@covSEiso,  @covSEiso,@covSEiso}, xg};
hyp0.cov = log([.1  1 .1 1 .1 1]);
y = counts(:);

X = covGrid('expand', cvGrid{3});
Idx = (1:length(y))';
lik = {@likPoisson, 'exp'};
hyp0.mean = .5;

%% learning
tic
hyp2 = minimize(hyp0, @gp, -100, @infGrid_Laplace, @meanConst, cvGrid, lik, Idx, y);
toc
exp(hyp2.cov)
%% inference
fprintf('Log-likelihood learned with Kronecker: %.04f\n', gp(hyp2, @infLaplace, @meanConst, cv, lik, X, y));
[Ef2, Varf2, fmu2, fs2, ll2] = gp(hyp2, @infGrid_Laplace, @meanConst, cvGrid, lik, Idx, y, Idx, y);
%%
% figure(1); imagesc([0 1], [0 1], (reshape(Ef1,32,32)')); set(gca,'YDir','normal'); colorbar;
figure(2); imagesc([0 1], [0 1], (reshape(Ef2,32,32)')); set(gca,'YDir','normal'); colorbar;