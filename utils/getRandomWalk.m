function x = getRandomWalk(eps, T, x0)
% random walk of length T with speed given by eps
% within unit box, reflection at borders

if nargin <3
    x0 = rand(2,1);
end

xt=x0;
x = zeros(T,2);
x(1,:)  = xt;
for i=2:T
    dx = eps*randn(2,1);
    xt = xt+(2*(dx+xt > 0)-1).* (2*(dx+xt < 1)-1).*dx;
    x(i,:) =xt;
end